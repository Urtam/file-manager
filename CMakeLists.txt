cmake_minimum_required(VERSION 3.8)
project(FileManager)

set (CMAKE_CXX_STANDART 16)

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(SOURCE_FILE main.cpp mainwindow.cpp mainwindow.h resourceap.qrc)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

add_executable(FileManager "main.cpp" "mainwindow.cpp" "mainwindow.h" "resourceap.qrc") 

target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Gui)
target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
