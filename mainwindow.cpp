#include "mainwindow.h"
#include <QGridLayout>
#include <QString>
#include <QDir>
#include <QMessageBox>
#include <QProcess>
#include <QList>
#include <QMimeData>
#include <QHeaderView>
#include <QMenuBar>
#include <QAction>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{    
    setWindowTitle(tr("File Manager"));
    setMinimumSize(1000, 500);
    setAcceptDrops(true);
    setWindowIcon(QIcon(":/images/icon until(setWindowIcon).png"));

    QMenu *fileMenu = menuBar()->addMenu("File");
    QMenu *helpMenu =menuBar()->addMenu("Help");
    fileMenu->addAction("Exit", this, SLOT(Exit()));
    helpMenu->addAction("About the program", this, SLOT(About_the_program()));

    box = new QComboBox(this);
    buttonConnect = new QPushButton("Connect to server", this);

    treeWidget = new QTreeWidget(this);
    treeWidget->setAnimated(true);
    treeWidget->setRootIsDecorated(0);
    treeWidget->setTabKeyNavigation(true);
    treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    treeWidget->setDragEnabled(true);
    treeWidget->setAcceptDrops(true);
    treeWidget->setDropIndicatorShown(true);
    treeWidget->setDragDropMode(QAbstractItemView::InternalMove);

    file = new QFile("SshServer.json");
    dir = new QDir;
    check = "";
    process = new QProcess(this);    

    QWidget *wid = new QWidget;
    setCentralWidget(wid);

    QGridLayout *grid = new QGridLayout;
    grid -> addWidget(box, 0, 0);
    grid -> addWidget(buttonConnect, 0, 1);
    grid -> addWidget(treeWidget, 1, 0);
    wid -> setLayout(grid);

    treeWidget -> setHeaderLabels(QStringList() << "File name" << "Directoire");
    treeWidget ->setColumnWidth(0, MainWindow::width() / 2);    

    connect(buttonConnect, SIGNAL(clicked()), this, SLOT(Connect_to_server()));
    connect(treeWidget, SIGNAL(itemActivated( QTreeWidgetItem*, int)), this, SLOT(Click_an_item()));

    if (!file->open(QIODevice::ReadOnly))
    {
        QMessageBox::StandardButtons reply = QMessageBox::critical(this,
                                                                "File Manager: Error",
                                                                "Json file not found. Please place your Json file in the following directory: "
                                                                + dir->absolutePath(),
                                                         QMessageBox::Ok);
        if(reply == QMessageBox::Ok)
        {
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        Filling_the_structure(file);
        for (int j = 0; j < serverVector.count(); j++)
        {
            box -> addItem(serverVector[j].name);
        }
    }    
}

void MainWindow::Connect_to_server()
{

   for (int i = 0; i < serverVector.count(); i++)
    {
       if (box->currentText() == serverVector[i].name && check != box->currentText())
       {
           treeWidget->clear();
           stringList.clear();
           stringList.append("The main directory");
           QString root = dir->root().path();
           QTreeWidgetItem *people = new QTreeWidgetItem(2);           
           people->setText(0, "The main directory");
           people->setText(1, root);
           people->setIcon(0,QIcon(":/images/icon until(QTreeWidget).png"));
           printDir(root, people);
           treeWidget->setColumnCount(2);
           treeWidget->addTopLevelItem(people);
           check = box->currentText();

           /*process = new QProcess(this);
           process->start("ssh", QStringList() << "-p" + serverVector[i].port + serverVector[i].username + "@" + serverVector[i].address
                          << serverVector[i].password << "dir");

           if (process->state() == QProcess::NotRunning)
           {
                QMessageBox::critical(this, "File Manager: Connection", "Error connecting to " + box->currentText() + " server");
           }
           if (process->state() == QProcess::Running)
           {
                QMessageBox::information(this, "File Manager:  Connection", "The connection with the server " + box->currentText() + " installed");

                QByteArray data;
                process->waitForReadyRead(500);
                data = process->readAll();
                qDebug() << data;
           }*/
       }
    }
}

void MainWindow::printDir(QString path, QTreeWidgetItem *people)
{
    int check = 1;
    QDir folder(path);        

    for (QFileInfo temp : folder.entryInfoList())
    {
        if(check > 2)
        {            
            QTreeWidgetItem *child = new QTreeWidgetItem(2);
            child->setText(0,temp.fileName());
            child->setText(1, temp.absoluteFilePath());
            people->addChild(child);            
            if (temp.isDir())
            {
                child->setIcon(0,QIcon(":/images/icon until(QTreeWidget).png"));
            }
        }
        check++;
    }    
}

void MainWindow::Click_an_item()
{
    bool flag = false;
    for (int i = 0; i < stringList.count(); i++) {
        if (stringList.at(i) == treeWidget->currentItem()->text(0))
        {
            flag = true;
        }
    }
    if (!flag)
    {
        stringList.append(treeWidget->currentItem()->text(0));
        printDir(treeWidget->currentItem()->text(1), treeWidget->currentItem());
        flag = false;
    }
}

int MainWindow::Exit()
{
   exit(EXIT_FAILURE);
}

void MainWindow::About_the_program()
{
    QFile aboutFileManager(":/about/filemanager/About File Manager.txt");
    aboutFileManager.open(QIODevice::ReadOnly);
    QMessageBox::about(this, "File Manager:  About the program", aboutFileManager.readAll());
}

MainWindow::~MainWindow()
{

}
