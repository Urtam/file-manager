#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>

int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    QSplashScreen *spalsh = new QSplashScreen;
    spalsh->setPixmap(QPixmap (":/images/File-Manager(screen saver).png"));
    spalsh->show();
    MainWindow *w = new MainWindow;
    w->show();
    spalsh->finish(w);
    delete spalsh;

    return a.exec();
}
