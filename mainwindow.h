#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QPushButton>
#include <QTreeWidget>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QAbstractItemModel>
#include <QFile>
#include <QProcess>
#include <QDir>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    struct server
    {
       QString name;
       QString address;
       QString port;
       QString username;
       QString password;       
    };server sshServer;

    QVector<server> serverVector;

    void Filling_the_structure(QFile *file)
    {
        QJsonDocument Jdoc;
        QJsonArray JdocArray;
        QJsonParseError JError;       

        Jdoc = QJsonDocument::fromJson(QByteArray(file->readAll()), &JError);
        file->close();

        if (JError.errorString().toInt() == QJsonParseError::NoError)
        {
            JdocArray = QJsonValue(Jdoc.object().value("ssh_server")).toArray();
            for (int i = 0; i < JdocArray.count(); i++)
            {
                sshServer.name = JdocArray.at(i).toObject().value("name").toString();
                sshServer.address = JdocArray.at(i).toObject().value("address").toString();;
                sshServer.port = JdocArray.at(i).toObject().value("port").toString();
                sshServer.username = JdocArray.at(i).toObject().value("username").toString();;
                sshServer.password = JdocArray.at(i).toObject().value("password").toString();;
                serverVector.push_back(sshServer);
            }
        }
    }

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:    
    QComboBox *box;
    QPushButton *buttonConnect;
    QTreeWidget *treeWidget;
    QFile *file;    
    QDir *dir;
    QString lastButton;
    QString check;
    QStringList stringList;
    QProcess *process;

private slots:
    void Connect_to_server();
    void Click_an_item();
    void printDir(QString path, QTreeWidgetItem *people);
    int Exit();
    void About_the_program();
};

#endif // MAINWINDOW_H
